# coding: utf-8

import cgi
import urllib
from functools import partial
import re
from kivy.animation import Animation
from kivy.app import App
from kivy.clock import Clock
from kivy.core.window import Window
from kivy.properties import StringProperty, NumericProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import ScreenManager, Screen, SwapTransition, CardTransition
from kivy.uix.scrollview import ScrollView
from kivy.uix.slider import Slider
from kivy.uix.stacklayout import StackLayout
from kivy.uix.textinput import TextInput
from kivy.utils import platform
from kivy.uix.image import Image

from kivymd.textfields import MDTextField
from kivymd.button import MDIconButton
from kivymd.list import ILeftBody, ILeftBodyTouch, IRightBodyTouch, BaseListItem, TwoLineListItem
from kivymd.selectioncontrols import MDCheckbox
from kivymd.theming import ThemeManager

try:
    import cPickle as pickle
except ImportError:
    from geral import pickle
if platform == "win":
    Window.size = (415, 670)
elif platform == "linux":
    Window.size = (415, 670)
else:
    pass


class Geral():
    def __init__(self):
        pass

    def cria_arquivo(self, y, ):
        dicionario = {"nome": "",
                      "mae": "",
                      "pai": "",
                      "nascimento": "",
                      "nacionalidade": "BRASIL",
                      "estado": "vazio",
                      "abrevi": "",
                      "cidade": "",
                      "sexo": "",
                      "estadocivil": "",
                      "sangue": "",
                      "raca": "",
                      "cep": "",

                      }

        arquivo = open(y + ".txt", 'w')
        pickle.dump(dicionario, arquivo)
        arquivo.close()

    def altera_parametros(self, x, y, v):
        """acessa arquivo texto para alteração de parametros
        x = campo a ser alterado
        y = nome do arquivo txt a ser alterado
        v = dados a serem incluidos
        """
        arquivo = open(y + ".txt", 'r')
        linha = pickle.load(arquivo)
        linha[x] = v
        arquivo = open(y + ".txt", 'w')
        pickle.dump(linha, arquivo)
        arquivo.close()

    def busca_arquivo(self, campo, txt):
        """busca dados gravados no arquivo txt
        x = campo buscado
        y = nome do arquivo txt que esta gravado os dados
        """
        arquivo = open(txt + ".txt", 'r')
        linha = pickle.load(arquivo)
        arquivo.close()
        # print str(linha[campo])
        return str(linha[campo])


class ActionBar_1(Label):
    """
    action bar implementada em kv
    """
    pass


class Bt(ButtonBehavior, Label):
    """
    botao circular implementdado em kv com canvas
    """
    pass


class Botao(Button):
    """
    botao azul padronizado com código implementado em kv
    """
    pass


class BoxlayoutBranco(BoxLayout):
    """
    boxlayout fundo branco implementado em kv com canvas
    """
    pass


class BoxsScreenCad2(BoxLayout):
    cor_1 = StringProperty("e4efee")
    cor_2 = StringProperty("00a2e8")
    cor_3 = StringProperty("00a2e8")


class Bt_alfabeto(Botao):
    pass


class Bt_aviso(Button):
    pass


class Bt_cidade(Botao):
    pass


class Bt_close(Button):
    pass


class Bt_title(Button):
    pass


class Buto(Button):
    pass


class ButoBuscacep(Button):
    pass


class ButoCep(Button):
    pass


class ButoData(Button):
    pass


class Bt_estado(Button):
    pass


class Bt_estadocivil(Botao):
    pass


class Bt_nacionalidade(Button):
    pass


class Bt_naturalidade(Button):
    pass


class Bt_paises(Botao):
    pass


class Bt_raca(Botao):
    pass


class Bt_sangue(Botao):
    pass


class Bt_sexo(Botao):
    pass


class Buto2(Button):
    pass


class Buttonpadrao(Button):
    pass


class Cliente_option(Screen):
    pass


class Colaborador_option(Screen):
    pass


class FloatCep(FloatLayout):
    pass


class FloatData(FloatLayout):
    pass


class FloatTela_1(FloatLayout):
    def __init__(self, **kwargs):
        super(FloatTela_1, self).__init__(**kwargs)
        # self.popup_3 = Popup_3(self)

    def cria_popup(self, x, y):
        """
        x= lista
        y= titulo

        """
        self.poppp = Popup_3(title=y, size_hint=(0.8, .8), auto_dismiss=True)

        self.layout1 = StackLayout(orientation='lr-bt')
        self.closebutton = Bt_close(text="FECHAR", size_hint=(0.9, 0.08))
        self.closebutton.bind(on_press=partial(self.dismisss, x, ))
        self.scrlv = ScrollPopup(size_hint=(0.9, 0.80))
        self.s = Slider(min=0, max=1, value=25, orientation='vertical', step=0.01, size_hint=(0.1, 0.95))
        self.scrlv.bind(scroll_y=partial(self.slider_change, self.s))
        self.s.bind(value=partial(self.scroll_change, self.scrlv))
        self.layout = GridLayout(cols=3, spacing=4, size_hint_y=None, )
        self.layout.bind(minimum_height=self.layout.setter('height'))
        self.scrlv.add_widget(self.layout)
        self.layout1.add_widget(self.closebutton)
        self.layout1.add_widget(self.scrlv)
        self.layout1.add_widget(self.s)
        # self.add_widget(self.layout1)
        self.poppp.add_widget(self.layout1)

        # Geral().cria_arquivo("par_")
        # self.layout.clear_widgets()
        if x == "estados":
            if Geral().busca_arquivo("nacionalidade", "par_") != "BRASIL":
                self.poppp.open()
                self.layout.add_widget(Label(text="NÃO É NECESSÁRIO O PREENCHIMENTO EM CASO DE ESTRANGEIRO."))
            else:
                self.busca_estado()

        elif x == "paises":

            self.layout.cols = 3
            pp = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
                  "U",
                  "W", "Y", "Z"]
            self.poppp.open()
            for line in pp:
                self.bt = Bt_alfabeto(text=str(line), size_hint_y=None)
                self.layout.add_widget(self.bt)
                self.bt.bind(on_press=partial(self.busca_pais, self.bt.text, x))

        elif x == "sexo":
            self.poppp.open()
            self.poppp.size_hint = (0.8, .5)
            self.layout.cols = 1
            listsexo = ["MASCULINO", "FEMININO"]
            for i in listsexo:
                self.bt_sexo = Bt_sexo(text=str(i), size_hint_y=None)
                self.layout.add_widget(self.bt_sexo)
                self.bt_sexo.bind(on_press=self.poppp.dismiss)

        elif x == "raca":
            self.poppp.open()
            self.layout.cols = 1
            listraca = ["BRANCA", "NEGRA", "AMARELA", "PARDA", "NÃO INFORMADO"]
            for i in listraca:
                self.bt_raca = Bt_raca(text=str(i), size_hint_y=None)
                self.layout.add_widget(self.bt_raca)
                self.bt_raca.bind(on_press=self.poppp.dismiss)

        elif x == "cidades":

            if Geral().busca_arquivo("nacionalidade", "par_") != "BRASIL":
                self.poppp.open()
                self.layout.add_widget(Label(text="NÃO É NECESSÁRIO O PREENCHIMENTO EM CASO DE ESTRANGEIRO."))


            elif Geral().busca_arquivo("estado", "par_") == "vazio":
                self.poppp.open()
                self.layout.add_widget(Label(text="É NECESSÁRIO ESCOLHER O ESTADO ANTES DA CIDADE."))
            else:
                # print Geral().busca_arquivo("estado","par_")
                self.layout.cols = 3
                pp = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S",
                      "T",
                      "U",
                      "W", "Y", "Z"]
                self.poppp.open()
                for line in pp:
                    self.bt = Bt_cidade(text=str(line), size_hint_y=None)
                    self.layout.add_widget(self.bt)
                    self.bt.bind(on_press=partial(self.busca_cidade, self.bt.text, x))





                    # pass
                    # self.busca_cidade()
                    # print Geral().busca_arquivo("nome","par_")

        elif x == "estadocivil":
            self.poppp.open()
            self.poppp.size_hint = (0.8, .8)
            self.layout.cols = 1
            listsexo = ["CASADO", "DESQUITADO", "DIVORCIADO", "SOLTEIRO", "UNIÃO ESTÁVEL", "VÍUVO", "OUTROS"]
            for i in listsexo:
                self.bt_estadocivil = Bt_estadocivil(text=str(i), size_hint_y=None)
                self.layout.add_widget(self.bt_estadocivil)
                self.bt_estadocivil.bind(on_press=self.poppp.dismiss)


        elif x == "sangue":
            self.poppp.open()
            self.poppp.size_hint = (0.8, .8)
            self.layout.cols = 1
            listsexo = ["O+", "O-", "A+", "A-", "B+", "B-", "AB+", "AB-"]
            for i in listsexo:
                self.bt_sangue = Bt_sangue(text=str(i), size_hint_y=None)
                self.layout.add_widget(self.bt_sangue)
                self.bt_sangue.bind(on_press=self.poppp.dismiss)

    def dismisss(self, *args):
        """função define por condição se botao fecha popup ou se cria botão voltar"""
        arg0 = args[0]

        # print "argumento = " + arg1
        if self.closebutton.text == "FECHAR":
            # self.layout.clear_widgets()
            # self.layout.cols = 3
            # self.alfa_indice()
            self.poppp.dismiss()
        elif self.closebutton.text == "VOLTAR":
            self.closebutton.text = "FECHAR"
            self.layout.clear_widgets()
            self.layout.cols = 3
            self.alfa_indice(arg0)

    def time(self, ):
        Clock.schedule_once(self.alfa_indice, .2)
        self.lb = Label(text="AGUARDE SENHOR")
        self.layout.add_widget(self.lb)

    def alfa_indice(self, x):
        """função incluis botoes de a a z no popup
        x = especifica de onde vem o comando da funçao,
        busca cidades ou busca pais ou etc
        """
        self.layout.clear_widgets()
        self.layout.cols = 3
        pp = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
              "W", "Y", "Z"]

        if x == "paises:":

            for line in pp:
                self.bt = Bt_alfabeto(text=str(line), size_hint_y=None)
                self.layout.add_widget(self.bt)
                self.bt.bind(on_press=partial(self.busca_pais, self.bt.text, "paises", ))

        elif x == "cidades":

            for line in pp:
                self.bt = Bt_cidade(text=str(line), size_hint_y=None)
                self.layout.add_widget(self.bt)
                self.bt.bind(on_press=partial(self.busca_cidade, self.bt.text, "cidades"))

    def deleta_cidade(self, x):

        """exclui cidade escolhida ao selecionar novo estado"""
        # self.ids.txunfe.text = ""

    def busca_estado(self, ):
        """
        args[0] = letra para indice, nome da lista(exemplo, paises, estados, cidades)
        args[1] = Nome da lista(exemplo, paises, estados, cidades)
        função inclui botõe com nome dos países com base na instance.text da classe"""

        self.layout.cols = 1
        self.arquivo = open("estados.txt", "r")
        self.lista = self.arquivo.readlines()
        self.arquivo.close()
        self.poppp.open()
        for line in self.lista:
            x = line.split(";")

            self.bt_estado = Bt_estado(text=str(x[0]), size_hint_y=None)
            self.layout.add_widget(self.bt_estado)
            self.bt_estado.bind(on_press=partial(self.poppp.dismiss))
            # self.bt_paises.bind(on_press=partial(self.teste, x))
            # self.bt_paises.bind(on_press = partial(self.on_anything, "1", "2", monthy="python"))

    def busca_pais(self, *args):
        """
        args[0] = letra para indice, nome da lista(exemplo, paises, estados, cidades)
        args[1] = Nome da lista(exemplo, paises, estados, cidades)
        função inclui botõe com nome dos países com base na instance.text da classe"""

        self.lista = str(args[1])  # nome do arquivo texto
        self.letra = str(args[0])  # letra indice
        self.layout.clear_widgets()
        self.closebutton.text = "VOLTAR"
        self.layout.cols = 1
        self.arquivo = open(self.lista + ".txt", "r")
        self.lista = self.arquivo.readlines()
        self.arquivo.close()
        for line in self.lista:
            if line[0] == self.letra:
                self.bt_paises = Bt_paises(text=str(line), size_hint_y=None)
                self.layout.add_widget(self.bt_paises)
                self.bt_paises.bind(on_press=self.poppp.dismiss)

    def busca_cidade(self, *args):
        """
        args[0] = letra para indice, nome da lista(exemplo, paises, estados, cidades)
        args[1] = Nome da lista(exemplo, paises, estados, cidades)
        função inclui botõe com nome dos países com base na instance.text da classe"""
        self.lista = str(args[1])  # nome do arquivo texto
        self.letra = str(args[0])  # letra indice
        self.layout.clear_widgets()
        self.closebutton.text = "VOLTAR"
        self.layout.cols = 1
        self.arquivo = open(self.lista + ".txt", "r")
        self.lista = self.arquivo.readlines()
        self.arquivo.close()
        self.estadobusca = Geral().busca_arquivo("abrevi", "par_")
        for line in self.lista:
            l = line.split(";")
            if l[0] == self.estadobusca:
                if l[3][0] == self.letra:
                    self.bt_cidade = Bt_cidade(text=l[3], size_hint_y=None)
                    self.layout.add_widget(self.bt_cidade)
                    self.bt_cidade.bind(on_press=self.poppp.dismiss)

    def busca_cidade_2(self, x, y):
        # print x
        self.mx = x
        for line in y:

            print (self.x)
            # self.mx = x
            # print self.mx
            # print x
            l = line.split(";")
            # print x
            # print x[0]
            self.x = + self.x
            if l[0] == x:
                print (x[3])

    def teste(self, ):
        print("testado e aprovado")

    def on_anything(self, *args, **kwargs):
        print(str(args), "and **kwargs of", str(kwargs))

    def scroll_change(self, scrlv, instance, value):
        scrlv.scroll_y = value

    def slider_change(self, s, instance, value):
        if value >= 0:
            s.value = value

    def focus_text(self, x, y):
        """verifica se há foco no tex input, sé ele
        redimensiona os widgets text imput e a imagem
        para cima para não colidir com o teclado
        """
        self.medida = "14sp"  # medida padrao para rptulos do textinput

        if x[1] == True:

            self.ids.lbnome.size_hint_y = .0
            self.ids.txnome.size_hint_y = .0
            self.ids.lbnome.size_hint_x = .0
            self.ids.txnome.size_hint_x = .0
            self.ids.lbnome.pos_hint = {"x": .0, "y": .0}
            self.ids.txnome.pos_hint = {"x": .0, "y": .0}

            self.ids.lbnmae.size_hint_y = .0
            self.ids.txnmae.size_hint_y = .0
            self.ids.lbnmae.size_hint_x = .0
            self.ids.txnmae.size_hint_x = .0
            self.ids.lbnmae.pos_hint = {"x": .0, "y": .0}
            self.ids.txnmae.pos_hint = {"x": .0, "y": .0}

            self.ids.lbnpai.size_hint_y = .0
            self.ids.txnpai.size_hint_y = .0
            self.ids.lbnpai.size_hint_x = .0
            self.ids.txnpai.size_hint_x = .0
            self.ids.lbnpai.pos_hint = {"x": .0, "y": .0}
            self.ids.txnpai.pos_hint = {"x": .0, "y": .0}

            self.ids.lbdtns.size_hint_y = .0
            self.ids.txdtns.size_hint_y = .0
            self.ids.lbdtns.size_hint_x = .0
            self.ids.txdtns.size_hint_x = .0
            self.ids.lbdtns.pos_hint = {"x": .0, "y": .0}
            self.ids.txdtns.pos_hint = {"x": .0, "y": .0}

            self.ids.lbnaci.size_hint_y = .0
            self.ids.txnaci.size_hint_y = .0
            self.ids.lbnaci.size_hint_x = .0
            self.ids.txnaci.size_hint_x = .0
            self.ids.lbnaci.pos_hint = {"x": .0, "y": .0}
            self.ids.txnaci.pos_hint = {"x": .0, "y": .0}

            self.ids.lbnatu.size_hint_y = .0
            self.ids.txnatu.size_hint_y = .0
            self.ids.lbnatu.size_hint_x = .0
            self.ids.txnatu.size_hint_x = .0
            self.ids.lbnatu.pos_hint = {"x": .0, "y": .0}
            self.ids.txnatu.pos_hint = {"x": .0, "y": .0}

            self.ids.lbunfe.size_hint_y = .0
            self.ids.txunfe.size_hint_y = .0
            self.ids.lbunfe.size_hint_x = .0
            self.ids.txunfe.size_hint_x = .0
            self.ids.lbunfe.pos_hint = {"x": .0, "y": .0}
            self.ids.txunfe.pos_hint = {"x": .0, "y": .0}

            self.ids.lbsexo.size_hint_y = .0
            self.ids.txsexo.size_hint_y = .0
            self.ids.lbsexo.size_hint_x = .0
            self.ids.txsexo.size_hint_x = .0
            self.ids.lbsexo.pos_hint = {"x": .0, "y": .0}
            self.ids.txsexo.pos_hint = {"x": .0, "y": .0}

            self.ids.lbestc.size_hint_y = .0
            self.ids.txestc.size_hint_y = .0
            self.ids.lbestc.size_hint_x = .0
            self.ids.txestc.size_hint_x = .0
            self.ids.lbestc.pos_hint = {"x": .0, "y": .0}
            self.ids.txestc.pos_hint = {"x": .0, "y": .0}

            self.ids.lbtpsg.size_hint_y = .0
            self.ids.txtpsg.size_hint_y = .0
            self.ids.lbtpsg.size_hint_x = .0
            self.ids.txtpsg.size_hint_x = .0
            self.ids.lbtpsg.pos_hint = {"x": .0, "y": .0}
            self.ids.txtpsg.pos_hint = {"x": .0, "y": .0}

            self.ids.lbraca.size_hint_y = .0
            self.ids.txraca.size_hint_y = .0
            self.ids.lbraca.size_hint_x = .0
            self.ids.txraca.size_hint_x = .0
            self.ids.lbraca.pos_hint = {"x": .0, "y": .0}
            self.ids.txraca.pos_hint = {"x": .0, "y": .0}

            self.ids.lbccep.size_hint_y = .0
            self.ids.txccep.size_hint_y = .0
            self.ids.lbccep.size_hint_x = .0
            self.ids.txccep.size_hint_x = .0
            self.ids.lbccep.pos_hint = {"x": .0, "y": .0}
            self.ids.txccep.pos_hint = {"x": .0, "y": .0}

            self.ids.lbrrua.size_hint_y = .0
            self.ids.txrrua.size_hint_y = .0
            self.ids.lbrrua.size_hint_x = .0
            self.ids.txrrua.size_hint_x = .0
            self.ids.lbrrua.pos_hint = {"x": .0, "y": .0}
            self.ids.txrrua.pos_hint = {"x": .0, "y": .0}

            if y == "lbnome":
                self.ids.lbnome.font_size = "20sp"
                self.ids.lbnome.size_hint_y = .05
                self.ids.txnome.size_hint_y = .05
                self.ids.lbnome.size_hint_x = .92
                self.ids.txnome.size_hint_x = .92
                self.ids.lbnome.pos_hint = {"x": .04, "y": .67}
                self.ids.txnome.pos_hint = {"x": .04, "y": .62}


            elif y == "lbnmae":
                self.ids.lbnmae.font_size = "20sp"
                self.ids.lbnmae.size_hint_y = .05
                self.ids.txnmae.size_hint_y = .05
                self.ids.lbnmae.size_hint_x = .92
                self.ids.txnmae.size_hint_x = .92
                self.ids.lbnmae.pos_hint = {"x": .04, "y": .67}
                self.ids.txnmae.pos_hint = {"x": .04, "y": .62}

            elif y == "lbnpai":
                self.ids.lbnpai.font_size = "20sp"
                self.ids.lbnpai.size_hint_y = .05
                self.ids.txnpai.size_hint_y = .05
                self.ids.lbnpai.size_hint_x = .92
                self.ids.txnpai.size_hint_x = .92
                self.ids.lbnpai.pos_hint = {"x": .04, "y": .67}
                self.ids.txnpai.pos_hint = {"x": .04, "y": .62}


            elif y == "lbdtns":
                self.ids.lbdtns.font_size = "20sp"
                self.ids.lbdtns.size_hint_y = .05
                self.ids.txdtns.size_hint_y = .05
                self.ids.lbdtns.size_hint_x = .92
                self.ids.txdtns.size_hint_x = .92
                self.ids.lbdtns.pos_hint = {"x": .04, "y": .67}
                self.ids.txdtns.pos_hint = {"x": .04, "y": .62}

            elif y == "lbnaci":
                self.ids.lbnaci.font_size = "20sp"
                self.ids.lbnaci.size_hint_y = .05
                self.ids.txnaci.size_hint_y = .05
                self.ids.lbnaci.size_hint_x = .92
                self.ids.txnaci.size_hint_x = .92
                self.ids.lbnaci.pos_hint = {"x": .04, "y": .67}
                self.ids.txnaci.pos_hint = {"x": .04, "y": .62}

            elif y == "lbnatu":
                self.ids.lbnatu.font_size = "20sp"
                self.ids.lbnatu.size_hint_y = .05
                self.ids.txnatu.size_hint_y = .05
                self.ids.lbnatu.size_hint_x = .92
                self.ids.txnatu.size_hint_x = .92
                self.ids.lbnatu.pos_hint = {"x": .04, "y": .67}
                self.ids.txnatu.pos_hint = {"x": .04, "y": .62}

            elif y == "lbunfe":
                self.ids.lbunfe.font_size = "20sp"
                self.ids.lbunfe.size_hint_y = .05
                self.ids.txunfe.size_hint_y = .05
                self.ids.lbunfe.size_hint_x = .92
                self.ids.txunfe.size_hint_x = .92
                self.ids.lbunfe.pos_hint = {"x": .04, "y": .67}
                self.ids.txunfe.pos_hint = {"x": .04, "y": .62}

            elif y == "lbsexo":
                self.ids.lbsexo.font_size = "20sp"
                self.ids.lbsexo.size_hint_y = .05
                self.ids.txsexo.size_hint_y = .05
                self.ids.lbsexo.size_hint_x = .92
                self.ids.txsexo.size_hint_x = .92
                self.ids.lbsexo.pos_hint = {"x": .04, "y": .67}
                self.ids.txsexo.pos_hint = {"x": .04, "y": .62}

            elif y == "lbestc":
                self.ids.lbestc.font_size = "20sp"
                self.ids.lbestc.size_hint_y = .05
                self.ids.txestc.size_hint_y = .05
                self.ids.lbestc.size_hint_x = .92
                self.ids.txestc.size_hint_x = .92
                self.ids.lbestc.pos_hint = {"x": .04, "y": .67}
                self.ids.txestc.pos_hint = {"x": .04, "y": .62}

            elif y == "lbtpsg":
                self.ids.lbtpsg.font_size = "20sp"
                self.ids.lbtpsg.size_hint_y = .05
                self.ids.txtpsg.size_hint_y = .05
                self.ids.lbtpsg.size_hint_x = .92
                self.ids.txtpsg.size_hint_x = .92
                self.ids.lbtpsg.pos_hint = {"x": .04, "y": .67}
                self.ids.txtpsg.pos_hint = {"x": .04, "y": .62}

            elif y == "lbraca":
                self.ids.lbraca.font_size = "20sp"
                self.ids.lbraca.size_hint_y = .05
                self.ids.txraca.size_hint_y = .05
                self.ids.lbraca.size_hint_x = .92
                self.ids.txraca.size_hint_x = .92
                self.ids.lbraca.pos_hint = {"x": .04, "y": .67}
                self.ids.txraca.pos_hint = {"x": .04, "y": .62}

            elif y == "lbccep":
                self.ids.lbccep.font_size = "20sp"
                self.ids.lbccep.size_hint_y = .05
                self.ids.txccep.size_hint_y = .05
                self.ids.lbccep.size_hint_x = .92
                self.ids.txccep.size_hint_x = .92
                self.ids.lbccep.pos_hint = {"x": .04, "y": .67}
                self.ids.txccep.pos_hint = {"x": .04, "y": .62}

            elif y == "lbrrua":
                self.ids.lbrrua.font_size = "20sp"
                self.ids.lbrrua.size_hint_y = .05
                self.ids.txrrua.size_hint_y = .05
                self.ids.lbrrua.size_hint_x = .92
                self.ids.txrrua.size_hint_x = .92
                self.ids.lbrrua.pos_hint = {"x": .04, "y": .67}
                self.ids.txrrua.pos_hint = {"x": .04, "y": .62}





        elif x[1] == False:

            self.ids.lbnome.font_size = self.medida
            self.ids.lbnome.size_hint_y = .05
            self.ids.txnome.size_hint_y = .05
            self.ids.lbnome.size_hint_x = .92
            self.ids.txnome.size_hint_x = .92
            self.ids.lbnome.pos_hint = {"x": .04, "y": .87}
            self.ids.txnome.pos_hint = {"x": .04, "y": .82}

            self.ids.lbnmae.font_size = self.medida
            self.ids.lbnmae.size_hint_y = .05
            self.ids.txnmae.size_hint_y = .05
            self.ids.lbnmae.size_hint_x = .92
            self.ids.txnmae.size_hint_x = .92
            self.ids.lbnmae.pos_hint = {"x": .04, "y": .77}
            self.ids.txnmae.pos_hint = {"x": .04, "y": .72}

            self.ids.lbnpai.font_size = self.medida
            self.ids.lbnpai.size_hint_y = .05
            self.ids.txnpai.size_hint_y = .05
            self.ids.lbnpai.size_hint_x = .92
            self.ids.txnpai.size_hint_x = .92
            self.ids.lbnpai.pos_hint = {"x": .04, "y": .67}
            self.ids.txnpai.pos_hint = {"x": .04, "y": .62}

            self.ids.lbdtns.font_size = self.medida
            self.ids.lbdtns.size_hint_y = .05
            self.ids.txdtns.size_hint_y = .05
            self.ids.lbdtns.size_hint_x = .44
            self.ids.txdtns.size_hint_x = .44
            self.ids.lbdtns.pos_hint = {"x": .04, "y": .57}
            self.ids.txdtns.pos_hint = {"x": .04, "y": .52}

            self.ids.lbnaci.font_size = self.medida
            self.ids.lbnaci.size_hint_y = .05
            self.ids.txnaci.size_hint_y = .05
            self.ids.lbnaci.size_hint_x = .48
            self.ids.txnaci.size_hint_x = .46
            self.ids.lbnaci.pos_hint = {"x": .5, "y": .57}
            self.ids.txnaci.pos_hint = {"x": .5, "y": .52}

            self.ids.lbnatu.font_size = self.medida
            self.ids.lbnatu.size_hint_y = .05
            self.ids.txnatu.size_hint_y = .05
            self.ids.lbnatu.size_hint_x = .5
            self.ids.txnatu.size_hint_x = .5
            self.ids.lbnatu.pos_hint = {"x": .04, "y": .47}
            self.ids.txnatu.pos_hint = {"x": .04, "y": .42}

            self.ids.lbunfe.font_size = self.medida
            self.ids.lbunfe.size_hint_y = .05
            self.ids.txunfe.size_hint_y = .05
            self.ids.lbunfe.size_hint_x = .40
            self.ids.txunfe.size_hint_x = .40
            self.ids.lbunfe.pos_hint = {"x": .56, "y": .47}
            self.ids.txunfe.pos_hint = {"x": .56, "y": .42}

            self.ids.lbsexo.font_size = self.medida
            self.ids.lbsexo.size_hint_y = .05
            self.ids.txsexo.size_hint_y = .05
            self.ids.lbsexo.size_hint_x = .44
            self.ids.txsexo.size_hint_x = .44
            self.ids.lbsexo.pos_hint = {"x": .04, "y": .37}
            self.ids.txsexo.pos_hint = {"x": .04, "y": .32}

            self.ids.lbestc.font_size = self.medida
            self.ids.lbestc.size_hint_y = .05
            self.ids.txestc.size_hint_y = .05
            self.ids.lbestc.size_hint_x = .48
            self.ids.txestc.size_hint_x = .46
            self.ids.lbestc.pos_hint = {"x": .5, "y": .37}
            self.ids.txestc.pos_hint = {"x": .5, "y": .32}

            self.ids.lbtpsg.font_size = self.medida
            self.ids.lbtpsg.size_hint_y = .05
            self.ids.txtpsg.size_hint_y = .05
            self.ids.lbtpsg.size_hint_x = .44
            self.ids.txtpsg.size_hint_x = .44
            self.ids.lbtpsg.pos_hint = {"x": .04, "y": .27}
            self.ids.txtpsg.pos_hint = {"x": .04, "y": .22}

            self.ids.lbraca.font_size = self.medida
            self.ids.lbraca.size_hint_y = .05
            self.ids.txraca.size_hint_y = .05
            self.ids.lbraca.size_hint_x = .48
            self.ids.txraca.size_hint_x = .46
            self.ids.lbraca.pos_hint = {"x": .5, "y": .27}
            self.ids.txraca.pos_hint = {"x": .5, "y": .22}

            self.ids.lbccep.font_size = self.medida
            self.ids.lbccep.size_hint_y = .05
            self.ids.txccep.size_hint_y = .05
            self.ids.lbccep.size_hint_x = .44
            self.ids.txccep.size_hint_x = .44
            self.ids.lbccep.pos_hint = {"x": .04, "y": .17}
            self.ids.txccep.pos_hint = {"x": .04, "y": .12}

            self.ids.lbrrua.font_size = self.medida
            self.ids.lbrrua.size_hint_y = .05
            self.ids.txrrua.size_hint_y = .05
            self.ids.lbrrua.size_hint_x = .48
            self.ids.txrrua.size_hint_x = .46
            self.ids.lbrrua.pos_hint = {"x": .5, "y": .17}
            self.ids.txrrua.pos_hint = {"x": .5, "y": .12}


class FloatTela_2(GridLayout):
    def __init__(self, **kwargs):
        super(FloatTela_2, self).__init__(**kwargs)

    def clean(self):
        # self.text = TextUpeerMd2()
        self.remove_widget(self.ids.t)
        # self.add_widget(self.text, 8)


class GridPop(GridLayout):
    pass


class Lbanima(BoxLayout):
    pass


class Lbanima2(Label):
    pass


class Lbespaco(Label):
    pass


class L1(Label):
    pass


class L2(Label):
    pass


class LabelCep(Label):
    pass


class LabelData(Label):
    pass


class Login(Screen):
    def focus_text(self, x):
        """verifica se há foco no tex input, sé ele
        dedimensiona os widgets text imput e a imagem
        para cima para não colidir com o teclado
        """

        if x[1] == True:

            self.ids.imagem.pos_hint = {"x": .05, "y": .71}
            self.ids.imagem.size_hint_y = .25
            self.ids.login.pos_hint = {"x": .1, "y": .62}
            self.ids.senha.pos_hint = {"x": .1, "y": .52}
            self.ids.bt_text.pos_hint = {"x": .1, "y": .42}

        elif x[1] == False:

            self.ids.imagem.pos_hint = {"x": .05, "y": .57}
            self.ids.imagem.size_hint_y = .36
            self.ids.login.pos_hint = {"x": .1, "y": .43}
            self.ids.senha.pos_hint = {"x": .1, "y": .33}
            self.ids.bt_text.pos_hint = {"x": .1, "y": .23}


class MyPopup(Popup):
    pass


class PopupData(Popup):
    pass


class Popup_3(Popup):
    pass


class ScreenmangCadFunc(ScreenManager):
    def __init__(self, **kwargs):
        super(ScreenmangCadFunc, self).__init__(**kwargs)

    def screen_all_1(self):
        self.current = 'screen_all_1'

    def screen_all_2(self):
        self.current = 'screen_all_2'

    def screen_all_3(self):
        self.current = 'screen_all_3'


class Quemsomos(Screen):
    pass


class ScreenAll_1(Screen):
    pass


class ScreenAll_2(Screen):
    pass


class ScreenAll_3(Screen):
    pass


class ScreenCadastro_1(Screen):
    def __init__(self, **kwargs):
        super(ScreenCadastro_1, self).__init__(**kwargs)

    def time(self):
        Clock.schedule_interval(self.anima, 2)

    def anima(self, dt):
        # print self.ids.bt_1.pos_hint
        # print self.ids.lbanima.pos



        # anim = Animation(size=(0, 900), duration=2.)
        # self.lbanim = Lbanima()
        # self.lb = Lbanima2()
        # self.lbanim.add_widget(self.lb)
        # self.add_widget(self.lb)
        # anim = Animation(x=500) + Animation(size=(80, 80), duration=2.)

        # print self.ids.anim.text
        anim = Animation(pos=(0, 0), duration=2)
        # anim = Animation(pos=(100, 100), t='out_bounce')
        # anim += Animation(pos=(200, 100), t='out_bounce')
        # anim &= Animation(size=(500, 500))
        # anim += Animation(size=(100, 50))
        # anim.start(self.ids.anima)
        # anim = Animation(pos=(10, 350))
        # anim &= Animation(size=(500, 800), duration=2.)

        anim.start(self.ids.lbanima2)







        # def dd(self):
        #
        #
        #    serverHost = '192.168.0.9'  # se vazio o endereço é local
        #    serverPort = 40010
        #    mensagem = [b'Ola, bem vindo']
        #
        #    # cria objeto e onecta ao server
        #    sockobj = socket(AF_INET, SOCK_STREAM)  # AF_INET prottocolo TCP - SOCK_STREAN protocolo IP   TCP-IP
        #
        #    sockobj.connect((serverHost, serverPort))
        #
        #    for linha in mensagem:
        #        sockobj.send("ailton foi enviado")
        #
        #        # resposta do servidor
        #        data = sockobj.recv(1024)
        #        print "cliente recebeu,", data
        #
        #    sockobj.close()


class ScreenCadastro_2(Screen):
    pass


class Stack(StackLayout):
    pass


class Scroll(ScrollView):
    def __init__(self, **kwargs):
        super(Scroll, self).__init__(**kwargs)

    def clean(self):
        # self.text = TextUpeerMd2()
        # for child in self.ids.float2:
        # print(child)
        # self.add_widget(self.text, 8)
        self.text = TextUpeerMd(text="jjij")
        self.ids.float2.remove_widget(self.ids.t)
        self.ids.float2.add_widget(self.text, 9)


class ScrollPopup(ScrollView):
    """classe que implementa popup com scroll"""
    pass


class Text1(TextInput):
    pass


class TextUpeer(TextInput):
    # keyboard_suggestions = False

    def insert_text(self, substring, from_undo=False):
        s = substring.upper()
        self.multiline = False
        return super(TextUpeer, self).insert_text(s, from_undo=from_undo)
        # pass


class TextUpeerMd(MDTextField):
    # keyboard_suggestions = False


    def insert_text(self, substring, from_undo=False):
        s = substring.upper()
        self.multiline = False

        return super(TextUpeerMd, self).insert_text(s, from_undo=from_undo)


class TextFloatMd(MDTextField):
    """
    classe só aceita float
    """

    def insert_text(self, substring, from_undo=False):
        print substring
        s = substring
        return super(TextFloatMd, self).insert_text(s, from_undo=from_undo)


class TextFloatMd_Data(MDTextField):
    """
    classe só aceita float
    """

    # def insert_text(self, substring, from_undo=False):
    #    print substring
    #    s = substring
    #    return super(TextFloatMd, self).insert_text(s, from_undo=from_undo)
    pat = re.compile('[^0-9]')

    def insert_text(self, substring, from_undo=False):
        pat = self.pat
        if '.' in self.text:
            s = re.sub(pat, '', substring)
        else:

            s = '.'.join([re.sub(pat, '', s) for s in substring.split('.', 1)])

            self.m = self.text
            # s = s+"/"

        if len(self.m) == 1:
            # print self.m
            s = s + "/"

        elif len(self.m) == 4:
            # print self.m
            s = s + "/"

        elif len(self.m) == 10:
            # print self.m
            s = ""

        return super(TextFloatMd_Data, self).insert_text(s, from_undo=from_undo)


class TextUpeerMd_Senha(MDTextField):
    class TextUpeerMd(MDTextField):
        # keyboard_suggestions = False


        def insert_text(self, substring, from_undo=False):
            s = substring.upper()
            self.multiline = False

            return super(TextUpeerMd_Senha, self).insert_text(s, from_undo=from_undo)


class TextUpeerMd2(MDTextField):
    def __init__(self, **kwargs):
        super(TextUpeerMd2, self).__init__(**kwargs)

        self.input_type = 'number'

    def insert_text(self, substring, from_undo=False):
        if substring.isnumeric():
            if hasattr(self, "maxdigits"):
                if len(self.text) < self.maxdigits:
                    return super(TextUpeerMd2, self).insert_text(substring, from_undo=from_undo)
            else:
                return super(TextUpeerMd2, self).insert_text(substring, from_undo=from_undo)


class Two(TwoLineListItem):
    pass


class AvatarSampleWidget(ILeftBody, Image):
    pass


class IconLeftSampleWidget(ILeftBodyTouch, MDIconButton):
    pass


class IconRightSampleWidget(IRightBodyTouch, MDCheckbox):
    pass


class Principal(ScreenManager):
    def __init__(self, **kwargs):
        super(Principal, self).__init__(**kwargs)
        Window.bind(on_keyboard=self.Android_back_click)

    def mm(self):
        print ("ss")

    def alfa_indice(self, x):
        """função incluis botoes de a a z no popup
        x = especifica de onde vem o comando da funçao,
        busca cidades ou busca pais ou etc
        """
        self.layout.clear_widgets()
        self.layout.cols = 3
        pp = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
              "W", "Y", "Z"]

        if x == "paises:":

            for line in pp:
                self.bt = Bt_alfabeto(text=str(line), size_hint_y=None)
                self.layout.add_widget(self.bt)
                self.bt.bind(on_press=partial(self.busca_pais, self.bt.text, "paises", ))

        elif x == "cidades":

            for line in pp:
                self.bt = Bt_cidade(text=str(line), size_hint_y=None)
                self.layout.add_widget(self.bt)
                self.bt.bind(on_press=partial(self.busca_cidade, self.bt.text, "cidades"))

    def analise_data(self, x):
        """
        função analisa data
        :param x:22021981
        :return:
        """
        nun = 0
        y = " "
        for i in x:

            if x[nun] != y:
                nun = nun + 1



            elif x[nun] == y:
                return "ok2"

        dia = int(x[0] + x[1])
        mes = int(x[2] + x[3])
        ano = int(x[4] + x[5] + x[6] + x[7])

        valida = False

        # Meses com 31 dias

        if (mes == 1 or mes == 3 or mes == 5 or mes == 7 or
                    mes == 8 or mes == 10 or mes == 12):
            if (dia <= 31):
                valida = True
        # Meses com 30 dias

        elif (mes == 4 or mes == 6 or mes == 9 or mes == 11):
            if (dia <= 30):
                valida = True
        elif mes == 2:
            # Testa se é bissexto

            if (ano % 4 == 0 and ano % 400 != 0) or (ano % 400 == 0):
                if (dia <= 29):
                    valida = True
            elif (dia <= 28):
                valida = True

        if (valida):

            return "ok"
        else:
            return "erro"

    def Android_back_click(self, window, key, *largs):
        """Essa função tem como objetivo associar o pressionamento da tecla
         ESC ao botão de retorno do ANDROID , aodetectar o pressionamento da
         tecla ESC ou ao botão de retorno no android, ao detectar o pressionamento
        a função desencadia o acionamento do retorno para a tela anterior
        atravez das condicionais abaixo, o numero 27 refere-se a indentificação
        e associação da mesma ao botão de retorno do android"""
        if key == 27:  # detecta pressionamento de tecla

            if str(self.current) == 'screenCadastro_1':
                # self.transition = self.CardTransition()

                # self.transition.direction = 'right'
                self.screenCadastro_1()
                # return True

            elif str(self.current) == 'colaborador_option':
                # self.transition = SwapTransition()
                # self.transition.direction = 'right'
                self.screenCadastro_1()
                self.transition.direction = 'right'
                return True

            elif str(self.current) == 'cliente_option':
                # self.transition = SwapTransition()
                self.transition.direction = 'right'
                self.screenCadastro_1()
                return True

            elif str(self.current) == 'colaborador_option':
                # self.transition = SwapTransition()
                self.transition.direction = 'right'
                self.screenCadastro_1()
                return True



            elif str(self.current) == 'screenCadastro_2':
                # self.transition = SwapTransition()
                # self.transition.direction = 'right'
                self.screenCadastro_1()
                return True


            elif str(self.current) == 'login':
                self.transition = SwapTransition()
                self.transition.direction = 'right'
                self.screenCadastro_1()
                return True

            elif str(self.current) == 'quemsomos':
                # self.transition = SwapTransition()
                # self.transition.direction = 'right'
                self.screenCadastro_1()
                return True

            elif str(self.current) == 'senha_colabor':
                # self.transition = SwapTransition()
                self.transition.direction = 'right'
                self.screenCadastro_1()
                return True

    def busca_estado(self, ):
        """
        args[0] = letra para indice, nome da lista(exemplo, paises, estados, cidades)
        args[1] = Nome da lista(exemplo, paises, estados, cidades)
        função inclui botõe com nome dos países com base na instance.text da classe"""

        self.layout.cols = 1
        self.arquivo = open("estados.txt", "r")
        self.lista = self.arquivo.readlines()
        self.arquivo.close()
        self.poppp.open()
        for line in self.lista:
            x = line.split(";")
            height = "60dp"

            self.bt_estado = Bt_estado(text=str(x[0]), size_hint_y=None, height="30dp")
            self.layout.add_widget(self.bt_estado)
            self.bt_estado.bind(on_press=partial(self.poppp.dismiss))
            # self.bt_paises.bind(on_press=partial(self.teste, x))
            # self.bt_paises.bind(on_press = partial(self.on_anything, "1", "2", monthy="python"))

    def busca_pais(self, *args):
        """
        args[0] = letra para indice, nome da lista(exemplo, paises, estados, cidades)
        args[1] = Nome da lista(exemplo, paises, estados, cidades)
        função inclui botõe com nome dos países com base na instance.text da classe"""

        self.lista = str(args[1])  # nome do arquivo texto
        self.letra = str(args[0])  # letra indice
        self.layout.clear_widgets()
        self.closebutton.text = "VOLTAR"
        self.layout.cols = 1
        self.arquivo = open(self.lista + ".txt", "r")
        self.lista = self.arquivo.readlines()
        self.arquivo.close()
        for line in self.lista:

            if line[0] == self.letra:
                line = line.replace("\n", "")
                self.bt_paises = Bt_paises(text=str(line), size_hint_y=None, height="30dp")
                self.layout.add_widget(self.bt_paises)
                self.bt_paises.bind(on_press=self.poppp.dismiss)

    def busca_cidade(self, *args):
        """
        args[0] = letra para indice, nome da lista(exemplo, paises, estados, cidades)
        args[1] = Nome da lista(exemplo, paises, estados, cidades)
        função inclui botõe com nome dos países com base na instance.text da classe"""
        self.lista = str(args[1])  # nome do arquivo texto
        self.letra = str(args[0])  # letra indice
        self.layout.clear_widgets()
        self.closebutton.text = "VOLTAR"
        self.layout.cols = 1
        self.arquivo = open(self.lista + ".txt", "r")
        self.lista = self.arquivo.readlines()
        self.arquivo.close()
        self.estadobusca = Geral().busca_arquivo("abrevi", "par_")
        for line in self.lista:
            l = line.split(";")
            if l[0] == self.estadobusca:
                if l[3][0] == self.letra:
                    self.bt_cidade = Bt_cidade(text=l[3], size_hint_y=None, height="30dp")
                    self.layout.add_widget(self.bt_cidade)
                    self.bt_cidade.bind(on_press=self.poppp.dismiss)

    def busca_cidade_2(self, x, y):
        # print x
        self.mx = x
        for line in y:

            print (self.x)
            # self.mx = x
            # print self.mx
            # print x
            l = line.split(";")
            # print x
            # print x[0]
            self.x = + self.x
            if l[0] == x:
                print (x[3])

    def buscacep(self, cep_busca):
        url = "http://cep.republicavirtual.com.br/web_cep.php?cep=" + cep_busca + "&formato=query_string"
        pagina = urllib.urlopen(url)
        conteudo = pagina.read()
        resultado = cgi.parse_qs(conteudo)

        if resultado['resultado'][0] == '1':
            y = resultado['tipo_logradouro'][0]
            x = resultado['logradouro'][0]

            return y + " " + x
        elif resultado['resultado'][0] == '2':
            print ("Endereço com cidade de CEP único: ")
            # print resultado['cidade'][0]
            # print resultado['uf'][0]
        else:
            return "CEP NÃO ENCONTRADO"


    def conta_letras(self, ):
        """
        funcao apenas se conecta a SqlFuncao, a funcao por sua vez
        verifica conexão com a internet e acessa o BD caso haja conexao,
        caso haja caracters a menos aplica condição de bloqueio conforme condicional
        abaixo
        """

        y = self.ids.login.text
        v = self.ids.senha.text
        if len(y) > 3:
            if len(v) > 5:
                # m = SqlFuncao()
                # return m.mysql_query_login(y, v)
                print ("ok")
            else:
                return "faltacaracter"
        else:
            return "faltacaracter"

    def cria_arquivo(self):
        Geral().cria_arquivo("par_")
        print ("arquivo criado")


        # def cria_pop(self):
        # self.cria_popup(x="paises", y ="NACIONALIDADE")

    def cria_popup(self, x, y):
        """
        x= lista
        y= titulo

        """
        self.poppp = Popup_3(title=y, size_hint=(0.8, 0.7), auto_dismiss=True)
        self.layout1 = Stack(orientation='lr-bt')
        self.scrlv = ScrollPopup(size_hint=(0.9, 0.9))
        self.s = Slider(min=0, max=1, value=25, orientation='vertical', step=0.01, size_hint=(0.1, 0.95))
        self.closebutton = Bt_close(text="FECHAR", size_hint=(0.9, 0.08))
        self.layout = GridPop(cols=3, spacing=2, size_hint_y=None, )

        self.closebutton.bind(on_press=partial(self.dismisss, x, ))
        self.scrlv.bind(scroll_y=partial(self.slider_change, self.s))
        self.s.bind(value=partial(self.scroll_change, self.scrlv))

        self.layout.bind(minimum_height=self.layout.setter('height'))
        self.scrlv.add_widget(self.layout)
        self.layout1.add_widget(self.closebutton)
        self.layout1.add_widget(self.scrlv)
        self.layout1.add_widget(self.s)
        self.poppp.add_widget(self.layout1)

        if x == "estados":
            if Geral().busca_arquivo("nacionalidade", "par_") != "BRASIL":
                self.poppp.open()
                self.layout.add_widget(Label(text="NÃO É NECESSÁRIO O PREENCHIMENTO EM CASO DE ESTRANGEIRO."))
            else:
                self.busca_estado()

        elif x == "paises":
            self.layout.cols = 3
            pp = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
                  "U",
                  "W", "Y", "Z"]
            self.poppp.open()
            for line in pp:
                self.bt = Botao(text=str(line), size_hint_y=None, height="60dp")
                self.layout.add_widget(self.bt)
                self.bt.bind(on_press=partial(self.busca_pais, self.bt.text, x))

        elif x == "sexo":
            self.poppp.open()
            self.poppp.size_hint = (0.8, .5)
            self.layout.cols = 1
            listsexo = ["MASCULINO", "FEMININO"]
            for i in listsexo:
                self.bt_sexo = Bt_sexo(text=str(i), size_hint_y=None)
                self.layout.add_widget(self.bt_sexo)
                self.bt_sexo.bind(on_press=self.poppp.dismiss)

        elif x == "raca":
            self.poppp.open()
            self.poppp.size_hint = (0.8, .5)
            self.layout.cols = 1
            listraca = ["BRANCA", "NEGRA", "AMARELA", "PARDA", "NÃO INFORMADO"]
            for i in listraca:
                self.bt_raca = Bt_raca(text=str(i), size_hint_y=None, height="30dp")
                self.layout.add_widget(self.bt_raca)
                self.bt_raca.bind(on_press=self.poppp.dismiss)

        elif x == "cidades":

            if Geral().busca_arquivo("nacionalidade", "par_") != "BRASIL":
                self.poppp.open()
                self.layout.add_widget(Label(text="NÃO É NECESSÁRIO O PREENCHIMENTO EM CASO DE ESTRANGEIRO."))


            elif Geral().busca_arquivo("estado", "par_") == "vazio":
                self.poppp.open()
                self.layout.add_widget(Label(text="É NECESSÁRIO ESCOLHER O ESTADO ANTES DA CIDADE."))
            else:
                # print Geral().busca_arquivo("estado","par_")
                self.layout.cols = 3
                pp = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S",
                      "T",
                      "U",
                      "W", "Y", "Z"]
                self.poppp.open()
                for line in pp:
                    self.bt = Bt_cidade(text=str(line), size_hint_y=None, height="60dp")
                    self.layout.add_widget(self.bt)
                    self.bt.bind(on_press=partial(self.busca_cidade, self.bt.text, x))





                    # pass
                    # self.busca_cidade()
                    # print Geral().busca_arquivo("nome","par_")

        elif x == "estadocivil":
            self.poppp.open()
            self.poppp.size_hint = (0.8, .5)
            self.layout.cols = 1
            listsexo = ["CASADO", "DESQUITADO", "DIVORCIADO", "SOLTEIRO", "UNIÃO ESTÁVEL", "VÍUVO", "OUTROS"]
            for i in listsexo:
                self.bt_estadocivil = Bt_estadocivil(text=str(i), size_hint_y=None, height="30dp")
                self.layout.add_widget(self.bt_estadocivil)
                self.bt_estadocivil.bind(on_press=self.poppp.dismiss)

        elif x == "sangue":
            self.poppp.open()
            self.poppp.size_hint = (0.8, .6)
            self.layout.cols = 1
            listsexo = ["O+", "O-", "A+", "A-", "B+", "B-", "AB+", "AB-"]
            for i in listsexo:
                self.bt_sangue = Bt_sangue(text=str(i), size_hint_y=None, height="30dp")
                self.layout.add_widget(self.bt_sangue)
                self.bt_sangue.bind(on_press=self.poppp.dismiss)

    def cria_popup_cep(self):
        self.popup_cep = PopupData(title="CEP:", size_hint=(.99, .8), auto_dismiss=True,
                                   title_align="center")
        self.popup_cep.open()
        self.floatcep = FloatCep()
        self.label_cep = LabelCep()

        self.butoncep = ButoBuscacep(text="BUSCAR", pos_hint={"x": .70, "y": .02})

        self.floatcep.add_widget(self.label_cep)
        self.floatcep.add_widget(self.butoncep)
        self.popup_cep.add_widget(self.floatcep)

    def cria_popup_data(self, ):
        self.popup_data = PopupData(title="FORMATO:   DD /MM / AAAA", size_hint=(.50, .50), auto_dismiss=True,
                                    title_align="center")
        self.popup_data.open()
        self.floatdata = FloatData()
        self.label_data = LabelData()

        self.butondata = ButoData(text="GRAVAR",
                                  pos_hint={"x": .70, "y": .02})  # associar (criar) algoriimo que analise a data
        self.floatdata.add_widget(self.label_data)
        self.floatdata.add_widget(self.butondata)
        self.popup_data.add_widget(self.floatdata)

    def cria_popup_senha(self, x):
        content = Bt_aviso(text=x)
        self.popup_senha = PopupData(title="Aviso", size_hint=(.7, .4),
                                     auto_dismiss=True,
                                     title_align="center",
                                     content=content)

        content.bind(on_press=self.popup_senha.dismiss)
        # self.canvas.add(Color(1., 1., 0))
        # self.canvas.add(Color(1., 1., 0))
        # self.canvas.add(Rectangle(size=(50, 50)))
        self.popup_senha.open()

    def deleta_cidade(self, x):

        """exclui cidade escolhida ao selecionar novo estado"""
        # self.ids.txunfe.text = ""

    def dismisss(self, *args):
        """função define por condição se botao fecha popup ou se cria botão voltar"""
        arg0 = args[0]
        # self.ids.textnac.keyboard_mode = "auto"
        # self.ids.textnac.focus = False

        # print "argumento = " + arg1
        if self.closebutton.text == "FECHAR":
            # self.layout.clear_widgets()
            # self.layout.cols = 3
            # self.alfa_indice()
            self.poppp.dismiss()
        elif self.closebutton.text == "VOLTAR":
            self.closebutton.text = "FECHAR"
            self.layout.clear_widgets()
            self.layout.cols = 3
            self.alfa_indice(arg0)

    def focus_text(self, x, y):
        """verifica se há foco no tex input, sé ele
        redimensiona os widgets text imput e a imagem
        para cima para não colidir com o teclado
        """
        self.medida = "14sp"  # medida padrao para rptulos do textinput

        if x[1] == True:

            self.ids.lbnome.size_hint_y = .0
            self.ids.txnome.size_hint_y = .0
            self.ids.lbnome.size_hint_x = .0
            self.ids.txnome.size_hint_x = .0
            self.ids.lbnome.pos_hint = {"x": .0, "y": .0}
            self.ids.txnome.pos_hint = {"x": .0, "y": .0}

            self.ids.lbnmae.size_hint_y = .0
            self.ids.txnmae.size_hint_y = .0
            self.ids.lbnmae.size_hint_x = .0
            self.ids.txnmae.size_hint_x = .0
            self.ids.lbnmae.pos_hint = {"x": .0, "y": .0}
            self.ids.txnmae.pos_hint = {"x": .0, "y": .0}

            self.ids.lbnpai.size_hint_y = .0
            self.ids.txnpai.size_hint_y = .0
            self.ids.lbnpai.size_hint_x = .0
            self.ids.txnpai.size_hint_x = .0
            self.ids.lbnpai.pos_hint = {"x": .0, "y": .0}
            self.ids.txnpai.pos_hint = {"x": .0, "y": .0}

            self.ids.lbdtns.size_hint_y = .0
            self.ids.txdtns.size_hint_y = .0
            self.ids.lbdtns.size_hint_x = .0
            self.ids.txdtns.size_hint_x = .0
            self.ids.lbdtns.pos_hint = {"x": .0, "y": .0}
            self.ids.txdtns.pos_hint = {"x": .0, "y": .0}

            self.ids.lbnaci.size_hint_y = .0
            self.ids.txnaci.size_hint_y = .0
            self.ids.lbnaci.size_hint_x = .0
            self.ids.txnaci.size_hint_x = .0
            self.ids.lbnaci.pos_hint = {"x": .0, "y": .0}
            self.ids.txnaci.pos_hint = {"x": .0, "y": .0}

            self.ids.lbnatu.size_hint_y = .0
            self.ids.txnatu.size_hint_y = .0
            self.ids.lbnatu.size_hint_x = .0
            self.ids.txnatu.size_hint_x = .0
            self.ids.lbnatu.pos_hint = {"x": .0, "y": .0}
            self.ids.txnatu.pos_hint = {"x": .0, "y": .0}

            self.ids.lbunfe.size_hint_y = .0
            self.ids.txunfe.size_hint_y = .0
            self.ids.lbunfe.size_hint_x = .0
            self.ids.txunfe.size_hint_x = .0
            self.ids.lbunfe.pos_hint = {"x": .0, "y": .0}
            self.ids.txunfe.pos_hint = {"x": .0, "y": .0}

            self.ids.lbsexo.size_hint_y = .0
            self.ids.txsexo.size_hint_y = .0
            self.ids.lbsexo.size_hint_x = .0
            self.ids.txsexo.size_hint_x = .0
            self.ids.lbsexo.pos_hint = {"x": .0, "y": .0}
            self.ids.txsexo.pos_hint = {"x": .0, "y": .0}

            self.ids.lbestc.size_hint_y = .0
            self.ids.txestc.size_hint_y = .0
            self.ids.lbestc.size_hint_x = .0
            self.ids.txestc.size_hint_x = .0
            self.ids.lbestc.pos_hint = {"x": .0, "y": .0}
            self.ids.txestc.pos_hint = {"x": .0, "y": .0}

            self.ids.lbtpsg.size_hint_y = .0
            self.ids.txtpsg.size_hint_y = .0
            self.ids.lbtpsg.size_hint_x = .0
            self.ids.txtpsg.size_hint_x = .0
            self.ids.lbtpsg.pos_hint = {"x": .0, "y": .0}
            self.ids.txtpsg.pos_hint = {"x": .0, "y": .0}

            self.ids.lbraca.size_hint_y = .0
            self.ids.txraca.size_hint_y = .0
            self.ids.lbraca.size_hint_x = .0
            self.ids.txraca.size_hint_x = .0
            self.ids.lbraca.pos_hint = {"x": .0, "y": .0}
            self.ids.txraca.pos_hint = {"x": .0, "y": .0}

            self.ids.lbccep.size_hint_y = .0
            self.ids.txccep.size_hint_y = .0
            self.ids.lbccep.size_hint_x = .0
            self.ids.txccep.size_hint_x = .0
            self.ids.lbccep.pos_hint = {"x": .0, "y": .0}
            self.ids.txccep.pos_hint = {"x": .0, "y": .0}

            self.ids.lbrrua.size_hint_y = .0
            self.ids.txrrua.size_hint_y = .0
            self.ids.lbrrua.size_hint_x = .0
            self.ids.txrrua.size_hint_x = .0
            self.ids.lbrrua.pos_hint = {"x": .0, "y": .0}
            self.ids.txrrua.pos_hint = {"x": .0, "y": .0}

            if y == "lbnome":
                self.ids.lbnome.font_size = "20sp"
                self.ids.lbnome.size_hint_y = .05
                self.ids.txnome.size_hint_y = .05
                self.ids.lbnome.size_hint_x = .92
                self.ids.txnome.size_hint_x = .92
                self.ids.lbnome.pos_hint = {"x": .04, "y": .67}
                self.ids.txnome.pos_hint = {"x": .04, "y": .62}


            elif y == "lbnmae":
                self.ids.lbnmae.font_size = "20sp"
                self.ids.lbnmae.size_hint_y = .05
                self.ids.txnmae.size_hint_y = .05
                self.ids.lbnmae.size_hint_x = .92
                self.ids.txnmae.size_hint_x = .92
                self.ids.lbnmae.pos_hint = {"x": .04, "y": .67}
                self.ids.txnmae.pos_hint = {"x": .04, "y": .62}

            elif y == "lbnpai":
                self.ids.lbnpai.font_size = "20sp"
                self.ids.lbnpai.size_hint_y = .05
                self.ids.txnpai.size_hint_y = .05
                self.ids.lbnpai.size_hint_x = .92
                self.ids.txnpai.size_hint_x = .92
                self.ids.lbnpai.pos_hint = {"x": .04, "y": .67}
                self.ids.txnpai.pos_hint = {"x": .04, "y": .62}


            elif y == "lbdtns":
                self.ids.lbdtns.font_size = "20sp"
                self.ids.lbdtns.size_hint_y = .05
                self.ids.txdtns.size_hint_y = .05
                self.ids.lbdtns.size_hint_x = .92
                self.ids.txdtns.size_hint_x = .92
                self.ids.lbdtns.pos_hint = {"x": .04, "y": .67}
                self.ids.txdtns.pos_hint = {"x": .04, "y": .62}

            elif y == "lbnaci":
                self.ids.lbnaci.font_size = "20sp"
                self.ids.lbnaci.size_hint_y = .05
                self.ids.txnaci.size_hint_y = .05
                self.ids.lbnaci.size_hint_x = .92
                self.ids.txnaci.size_hint_x = .92
                self.ids.lbnaci.pos_hint = {"x": .04, "y": .67}
                self.ids.txnaci.pos_hint = {"x": .04, "y": .62}

            elif y == "lbnatu":
                self.ids.lbnatu.font_size = "20sp"
                self.ids.lbnatu.size_hint_y = .05
                self.ids.txnatu.size_hint_y = .05
                self.ids.lbnatu.size_hint_x = .92
                self.ids.txnatu.size_hint_x = .92
                self.ids.lbnatu.pos_hint = {"x": .04, "y": .67}
                self.ids.txnatu.pos_hint = {"x": .04, "y": .62}

            elif y == "lbunfe":
                self.ids.lbunfe.font_size = "20sp"
                self.ids.lbunfe.size_hint_y = .05
                self.ids.txunfe.size_hint_y = .05
                self.ids.lbunfe.size_hint_x = .92
                self.ids.txunfe.size_hint_x = .92
                self.ids.lbunfe.pos_hint = {"x": .04, "y": .67}
                self.ids.txunfe.pos_hint = {"x": .04, "y": .62}

            elif y == "lbsexo":
                self.ids.lbsexo.font_size = "20sp"
                self.ids.lbsexo.size_hint_y = .05
                self.ids.txsexo.size_hint_y = .05
                self.ids.lbsexo.size_hint_x = .92
                self.ids.txsexo.size_hint_x = .92
                self.ids.lbsexo.pos_hint = {"x": .04, "y": .67}
                self.ids.txsexo.pos_hint = {"x": .04, "y": .62}

            elif y == "lbestc":
                self.ids.lbestc.font_size = "20sp"
                self.ids.lbestc.size_hint_y = .05
                self.ids.txestc.size_hint_y = .05
                self.ids.lbestc.size_hint_x = .92
                self.ids.txestc.size_hint_x = .92
                self.ids.lbestc.pos_hint = {"x": .04, "y": .67}
                self.ids.txestc.pos_hint = {"x": .04, "y": .62}

            elif y == "lbtpsg":
                self.ids.lbtpsg.font_size = "20sp"
                self.ids.lbtpsg.size_hint_y = .05
                self.ids.txtpsg.size_hint_y = .05
                self.ids.lbtpsg.size_hint_x = .92
                self.ids.txtpsg.size_hint_x = .92
                self.ids.lbtpsg.pos_hint = {"x": .04, "y": .67}
                self.ids.txtpsg.pos_hint = {"x": .04, "y": .62}

            elif y == "lbraca":
                self.ids.lbraca.font_size = "20sp"
                self.ids.lbraca.size_hint_y = .05
                self.ids.txraca.size_hint_y = .05
                self.ids.lbraca.size_hint_x = .92
                self.ids.txraca.size_hint_x = .92
                self.ids.lbraca.pos_hint = {"x": .04, "y": .67}
                self.ids.txraca.pos_hint = {"x": .04, "y": .62}

            elif y == "lbccep":
                self.ids.lbccep.font_size = "20sp"
                self.ids.lbccep.size_hint_y = .05
                self.ids.txccep.size_hint_y = .05
                self.ids.lbccep.size_hint_x = .92
                self.ids.txccep.size_hint_x = .92
                self.ids.lbccep.pos_hint = {"x": .04, "y": .67}
                self.ids.txccep.pos_hint = {"x": .04, "y": .62}

            elif y == "lbrrua":
                self.ids.lbrrua.font_size = "20sp"
                self.ids.lbrrua.size_hint_y = .05
                self.ids.txrrua.size_hint_y = .05
                self.ids.lbrrua.size_hint_x = .92
                self.ids.txrrua.size_hint_x = .92
                self.ids.lbrrua.pos_hint = {"x": .04, "y": .67}
                self.ids.txrrua.pos_hint = {"x": .04, "y": .62}





        elif x[1] == False:

            self.ids.lbnome.font_size = self.medida
            self.ids.lbnome.size_hint_y = .05
            self.ids.txnome.size_hint_y = .05
            self.ids.lbnome.size_hint_x = .92
            self.ids.txnome.size_hint_x = .92
            self.ids.lbnome.pos_hint = {"x": .04, "y": .87}
            self.ids.txnome.pos_hint = {"x": .04, "y": .82}

            self.ids.lbnmae.font_size = self.medida
            self.ids.lbnmae.size_hint_y = .05
            self.ids.txnmae.size_hint_y = .05
            self.ids.lbnmae.size_hint_x = .92
            self.ids.txnmae.size_hint_x = .92
            self.ids.lbnmae.pos_hint = {"x": .04, "y": .77}
            self.ids.txnmae.pos_hint = {"x": .04, "y": .72}

            self.ids.lbnpai.font_size = self.medida
            self.ids.lbnpai.size_hint_y = .05
            self.ids.txnpai.size_hint_y = .05
            self.ids.lbnpai.size_hint_x = .92
            self.ids.txnpai.size_hint_x = .92
            self.ids.lbnpai.pos_hint = {"x": .04, "y": .67}
            self.ids.txnpai.pos_hint = {"x": .04, "y": .62}

            self.ids.lbdtns.font_size = self.medida
            self.ids.lbdtns.size_hint_y = .05
            self.ids.txdtns.size_hint_y = .05
            self.ids.lbdtns.size_hint_x = .44
            self.ids.txdtns.size_hint_x = .44
            self.ids.lbdtns.pos_hint = {"x": .04, "y": .57}
            self.ids.txdtns.pos_hint = {"x": .04, "y": .52}

            self.ids.lbnaci.font_size = self.medida
            self.ids.lbnaci.size_hint_y = .05
            self.ids.txnaci.size_hint_y = .05
            self.ids.lbnaci.size_hint_x = .48
            self.ids.txnaci.size_hint_x = .46
            self.ids.lbnaci.pos_hint = {"x": .5, "y": .57}
            self.ids.txnaci.pos_hint = {"x": .5, "y": .52}

            self.ids.lbnatu.font_size = self.medida
            self.ids.lbnatu.size_hint_y = .05
            self.ids.txnatu.size_hint_y = .05
            self.ids.lbnatu.size_hint_x = .5
            self.ids.txnatu.size_hint_x = .5
            self.ids.lbnatu.pos_hint = {"x": .04, "y": .47}
            self.ids.txnatu.pos_hint = {"x": .04, "y": .42}

            self.ids.lbunfe.font_size = self.medida
            self.ids.lbunfe.size_hint_y = .05
            self.ids.txunfe.size_hint_y = .05
            self.ids.lbunfe.size_hint_x = .40
            self.ids.txunfe.size_hint_x = .40
            self.ids.lbunfe.pos_hint = {"x": .56, "y": .47}
            self.ids.txunfe.pos_hint = {"x": .56, "y": .42}

            self.ids.lbsexo.font_size = self.medida
            self.ids.lbsexo.size_hint_y = .05
            self.ids.txsexo.size_hint_y = .05
            self.ids.lbsexo.size_hint_x = .44
            self.ids.txsexo.size_hint_x = .44
            self.ids.lbsexo.pos_hint = {"x": .04, "y": .37}
            self.ids.txsexo.pos_hint = {"x": .04, "y": .32}

            self.ids.lbestc.font_size = self.medida
            self.ids.lbestc.size_hint_y = .05
            self.ids.txestc.size_hint_y = .05
            self.ids.lbestc.size_hint_x = .48
            self.ids.txestc.size_hint_x = .46
            self.ids.lbestc.pos_hint = {"x": .5, "y": .37}
            self.ids.txestc.pos_hint = {"x": .5, "y": .32}

            self.ids.lbtpsg.font_size = self.medida
            self.ids.lbtpsg.size_hint_y = .05
            self.ids.txtpsg.size_hint_y = .05
            self.ids.lbtpsg.size_hint_x = .44
            self.ids.txtpsg.size_hint_x = .44
            self.ids.lbtpsg.pos_hint = {"x": .04, "y": .27}
            self.ids.txtpsg.pos_hint = {"x": .04, "y": .22}

            self.ids.lbraca.font_size = self.medida
            self.ids.lbraca.size_hint_y = .05
            self.ids.txraca.size_hint_y = .05
            self.ids.lbraca.size_hint_x = .48
            self.ids.txraca.size_hint_x = .46
            self.ids.lbraca.pos_hint = {"x": .5, "y": .27}
            self.ids.txraca.pos_hint = {"x": .5, "y": .22}

            self.ids.lbccep.font_size = self.medida
            self.ids.lbccep.size_hint_y = .05
            self.ids.txccep.size_hint_y = .05
            self.ids.lbccep.size_hint_x = .44
            self.ids.txccep.size_hint_x = .44
            self.ids.lbccep.pos_hint = {"x": .04, "y": .17}
            self.ids.txccep.pos_hint = {"x": .04, "y": .12}

            self.ids.lbrrua.font_size = self.medida
            self.ids.lbrrua.size_hint_y = .05
            self.ids.txrrua.size_hint_y = .05
            self.ids.lbrrua.size_hint_x = .48
            self.ids.txrrua.size_hint_x = .46
            self.ids.lbrrua.pos_hint = {"x": .5, "y": .17}
            self.ids.txrrua.pos_hint = {"x": .5, "y": .12}

    def fecha_popup_data(self):
        """função fecha popup"""
        self.popup_data.dismiss()

    def on_anything(self, *args, **kwargs):
        print(str(args), "and **kwargs of", str(kwargs))

    def pop_exit(self, *args):
        self.poppp.dismiss()
        # self.ids.float2.clean()
        # self.ids.float2.clear_widgets(children=None)
        # self.ids.float2.clear_widgets()
        # print self.ids.float2
        # self.ids.textnac.is_focusable = True
        # elf.remove_widget()
        # root = BoxLayout()
        # ... add widgets to root ...
        # self.m =FloatTela_2
        # for child in self.m:
        #    print(child)

    def scroll_change(self, scrlv, instance, value):
        scrlv.scroll_y = value

    def slider_change(self, s, instance, value):
        if value >= 0:
            s.value = value

    def test(self, value):
        print (value)

    def time(self, ):
        Clock.schedule_once(self.alfa_indice, .2)
        self.lb = Label(text="AGUARDE SENHOR")
        self.layout.add_widget(self.lb)

    def tester(self):
        print ("oo")

    ##############
    #  SCREENS   #
    ##############

    def cliente_option(self):
        self.current = 'cliente_option'

    def colaborador_option(self):
        self.current = 'colaborador_option'

    def login(self):
        self.current = 'login'

    def quemsomos(self):
        self.current = 'quemsomos'

    def screenCadastro_1(self):
        self.current = 'screenCadastro_1'

    def screenCadastro_2(self):
        self.current = 'screenCadastro_2'

    def senha_colabor(self):
        self.current = 'senha_colabor'


class MainApp(App):
    center_y = NumericProperty(606)

    theme_cls = ThemeManager()
    theme_cls.primary_palette = 'Blue'
    title = "Example Text Fields"
    main_widget = None

    sexo = StringProperty("")
    nacionalidade = StringProperty("")
    estado = StringProperty("")
    estadocivil = StringProperty("")
    cidade = StringProperty("")
    raca = StringProperty("")
    sangue = StringProperty("")
    avisodata = StringProperty("")
    avisocep = StringProperty("")
    data0 = StringProperty(" ")
    data1 = StringProperty(" ")
    data2 = StringProperty(" ")
    data3 = StringProperty(" ")
    data4 = StringProperty(" ")
    data5 = StringProperty(" ")
    data6 = StringProperty(" ")
    data7 = StringProperty(" ")
    cep0 = StringProperty(" ")
    cep1 = StringProperty(" ")
    cep2 = StringProperty(" ")
    cep3 = StringProperty(" ")
    cep4 = StringProperty(" ")
    cep5 = StringProperty(" ")
    cep6 = StringProperty(" ")
    cep7 = StringProperty(" ")
    rua = StringProperty("")

    med = NumericProperty(10)
    anima_y = NumericProperty(0)

    KEY = StringProperty("0")
    MDIconButton()

    cor_1 = StringProperty("000000")
    cor_2 = StringProperty("ffffff")

    def on_nacionalidade(self, instance, value):
        print('My property a changed to', value)

    def grava(self, x, y):
        """função grava em txt utilizando pickle dados escolhido no cadastro
        x = arquivo texto a ser gravado
        y = nome do campo para gravacao do arquivo texto
        """

        if y == "estado":
            Geral().altera_parametros("estado", "par_", x)
            self.arquivo = open("estados.txt", "r")
            self.lista = self.arquivo.readlines()
            self.arquivo.close()
            for line in self.lista:
                y = line.split(";")
                if y[0] == x:
                    Geral().altera_parametros("abrevi", "par_", str(y[1][0:2]))

        elif y == "nacionalidade":
            Geral().altera_parametros(y, "par_", x[0:6])
            Geral().altera_parametros("estado", "par_", "vazio")

    def build(self):
        self.root = Principal()

        return self.root


if __name__ == '__main__':
    MainApp().run()
