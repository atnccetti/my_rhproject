from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.clock import Clock

import pymysql.cursors


class DB():
    # Connect to the database
    connection = pymysql.connect(host='www.db4free.net',
                                 user='jonasb',
                                 password='cogneasyhard',
                                 db='cogneasy',
                                 charset='utf8',
                                 cursorclass=pymysql.cursors.DictCursor)

    cursor = connection.cursor()

    def dados_botao(self):
        self.cursor.execute('SELECT id, nome, sobrenome FROM faculdades WHERE lixo = 0')

        dados = self.cursor.fetchall()

        dados_gerados = {}

        for i in range(len(dados)):
            di = dados[i]
            dados_gerados[di['id']] = {}

            dados_gerados[di['id']]['nome'] = di['nome']

            dados_gerados[di['id']]['nome2'] = di['sobrenome']

            self.cursor.execute(
                "SELECT AVG(nota) AS media FROM faculdades_avaliacoes WHERE id_faculdade = {}".format(di['id']))

            dados_gerados[di['id']]['media'] = self.cursor.fetchall()[0]['media']

            self.cursor.execute(
                "SELECT COUNT(id) AS avaliacoes FROM faculdades_avaliacoes WHERE id_faculdade = {}".format(di['id']))

            dados_gerados[di['id']]['avaliacoes'] = self.cursor.fetchall()[0]['avaliacoes']

            self.cursor.execute(
                "SELECT COUNT(id) AS comentarios FROM faculdades_comentarios WHERE id_faculdade = {}".format(di['id']))

            dados_gerados[di['id']]['comentarios'] = self.cursor.fetchall()[0]['comentarios']

        return dados_gerados

class Button1(Button):
    def _init_(self, nome='cu', **kwargs):
        super(Button1, self)._init_(**kwargs)

        #self.text = nome


class Box(BoxLayout):
    def _init_(self, **kwargs):
        super(Box, self)._init_(**kwargs)

        self.dados = DB().dados_botao()

        for key in self.dados:
            self.add_widget(Button1(
                self.dados[key]['nome']))





class Test(App):
    def build(self):
        return Box()


Test().run()