from struct import unpack
from os.path import expanduser

from zope.interface import Interface, implementer

from twisted.python.compat import nativeString, networkString
from twisted.python.filepath import FilePath
from twisted.python.failure import Failure
from twisted.internet.error import ConnectionDone, ProcessTerminated
from twisted.internet.interfaces import IStreamClientEndpoint
from twisted.internet.protocol import Factory
from twisted.internet.defer import Deferred, succeed, CancelledError
from twisted.internet.endpoints import TCP4ClientEndpoint, connectProtocol

from twisted.conch.ssh.keys import Key
from twisted.conch.ssh.common import NS
from twisted.conch.ssh.transport import SSHClientTransport
from twisted.conch.ssh.connection import SSHConnection
from twisted.conch.ssh.userauth import SSHUserAuthClient
from twisted.conch.ssh.channel import SSHChannel
from twisted.conch.client.knownhosts import ConsoleUI, KnownHostsFile
from twisted.conch.client.agent import SSHAgentClient
from twisted.conch.client.default import _KNOWN_HOSTS
from twisted.conch.endpoints import _NewConnectionHelper, _ExistingConnectionHelper,_CommandChannel



@implementer(IStreamClientEndpoint)
class SSHCommandClientEndpoint(object):
    """
    L{SSHCommandClientEndpoint} exposes the command-executing functionality of
    SSH servers.
    L{SSHCommandClientEndpoint} can set up a new SSH connection, authenticate
    it in any one of a number of different ways (keys, passwords, agents),
    launch a command over that connection and then associate its input and
    output with a protocol.
    It can also re-use an existing, already-authenticated SSH connection
    (perhaps one which already has some SSH channels being used for other
    purposes).  In this case it creates a new SSH channel to use to execute the
    command.  Notably this means it supports multiplexing several different
    command invocations over a single SSH connection.
    """

    def __init__(self, creator, command):
        """
        @param creator: An L{_ISSHConnectionCreator} provider which will be
            used to set up the SSH connection which will be used to run a
            command.
        @type creator: L{_ISSHConnectionCreator} provider
        @param command: The command line to execute on the SSH server.  This
            byte string is interpreted by a shell on the SSH server, so it may
            have a value like C{"ls /"}.  Take care when trying to run a
            command like C{"/Volumes/My Stuff/a-program"} - spaces (and other
            special bytes) may require escaping.
        @type command: L{bytes}
        """
        #self._creator = creator
        #self._command = command





    @classmethod
    def existingConnection(cls, connection, command):
        """
        Create and return a new endpoint which will try to open a new channel
        on an existing SSH connection and run a command over it.  It will
        B{not} close the connection if there is a problem executing the command
        or after the command finishes.
        @param connection: An existing connection to an SSH server.
        @type connection: L{SSHConnection}
        @param command: See L{SSHCommandClientEndpoint.newConnection}'s
            C{command} parameter.
        @type command: L{bytes}
        @return: A new instance of C{cls} (probably
            L{SSHCommandClientEndpoint}).
        """
        helper = _ExistingConnectionHelper(connection)
        return cls(helper, command)


    def connect(self, protocolFactory):
        """
        Set up an SSH connection, use a channel from that connection to launch
        a command, and hook the stdin and stdout of that command up as a
        transport for a protocol created by the given factory.
        @param protocolFactory: A L{Factory} to use to create the protocol
            which will be connected to the stdin and stdout of the command on
            the SSH server.
        @return: A L{Deferred} which will fire with an error if the connection
            cannot be set up for any reason or with the protocol instance
            created by C{protocolFactory} once it has been connected to the
            command.
        """
        d = self._creator.secureConnection()
        d.addCallback(self._executeCommand, protocolFactory)
        return d


    def _executeCommand(self, connection, protocolFactory):
        """
        Given a secured SSH connection, try to execute a command in a new
        channel created on it and associate the result with a protocol from the
        given factory.
        @param connection: See L{SSHCommandClientEndpoint.existingConnection}'s
            C{connection} parameter.
        @param protocolFactory: See L{SSHCommandClientEndpoint.connect}'s
            C{protocolFactory} parameter.
        @return: See L{SSHCommandClientEndpoint.connect}'s return value.
        """
        commandConnected = Deferred()

        def disconnectOnFailure(passthrough):
            # Close the connection immediately in case of cancellation, since
            # that implies user wants it gone immediately (e.g. a timeout):
            immediate = passthrough.check(CancelledError)
            self._creator.cleanupConnection(connection, immediate)
            return passthrough
        commandConnected.addErrback(disconnectOnFailure)

        channel = _CommandChannel(
            self._creator, self._command, protocolFactory, commandConnected)
        connection.openChannel(channel)
        return commandConnected




#newConnection(reactor=None, command="python gpio.py", username="pi", hostname='192.168.15.32', port=None,
#                      keys=None, password="raspberry", agentEndpoint=None,
#                      knownHosts=None, ui=None)