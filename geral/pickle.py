
#coding: utf-8
try:
    import cPickle as pickle
except ImportError:
    import pickle


class Texto():
    """classe contem funcoes para acesso a arquivos texto
    e manipulacao de parametros.
    """

    def cria_arquivo(self, y, ):
        """cria parametro para informar se esta logado ou nao
        x = dados em texto que serao incluidos no dicionario
        y = nome do arquivo txt que será gravado os dados

        """
        nacionalidade = ["BRASIL", "ARGENTINA", "DINAMARCA", "CHINA", "CHILE", "ESPANHA", "MADAGASCAR", "JAPÃO"]



        dicionario = {"nacionalidade": nacionalidade,
                      "empresa?": "",
                      "checkbox?": "",
                      "permanecer?": "",
                      "mes1?": "",
                      "mes2?":"",
                      }


        arquivo = open(y + ".txt", 'w')
        pickle.dump(dicionario, arquivo)
        arquivo.close()

    def adiciona_campo_parametro(self):
        pass

    def altera_parametros(self, x, y, v):
        """acessa arquivo texto para alteração de parametros
        x = campo a ser alterado
        y = nome do arquivo txt a ser alterado
        v = dados a serem incluidos
        """
        arquivo = open(y + ".txt", 'r')
        linha = pickle.load(arquivo)
        linha[x] = v
        arquivo = open(y + ".txt", 'w')
        pickle.dump(linha, arquivo)
        arquivo.close()

    def busca_arquivo(self, campo, txt):
        """busca dados gravados no arquivo txt
        x = campo buscado
        y = nome do arquivo txt que esta gravado os dados
        """
        arquivo = open(txt + ".txt", 'r')
        linha = pickle.load(arquivo)
        arquivo.close()
        return str(linha[campo])

    def nome_empresa(self):
        """busva no arquivo texto o nome da empresa"""
        arquivo = open("novo-arquivo.txt", "r")
        for row in arquivo:
            if row[0:3] == "emp":
                return str(row[4:])


x = Texto()
x.cria_arquivo(y = "paranmetros")
print (x.busca_arquivo("nacionalidade", "paranmetros" ))
